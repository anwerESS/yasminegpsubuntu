#include <cstring>
#include <fstream>
#include "fft.h"



void FFT::fft(complexVect *in, complexVect *out, int i_size, int conjugated)
{
    // create a DFT plan
    fftw_plan plan = fftw_plan_dft_1d(i_size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    // execute the plan
    fftw_execute(plan);
    // do some cleaning
    fftw_destroy_plan(plan);
    fftw_cleanup();

    if (COMPLEX_CONJUGATED)
        conjugate(out, i_size);
}

void FFT::ifft(complexVect *in, complexVect *out, int i_size)
{
    // create an IDFT plan
    fftw_plan plan = fftw_plan_dft_1d(i_size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
    // execute the plan
    fftw_execute(plan);
    // do some cleaning
    fftw_destroy_plan(plan);
    fftw_cleanup();
    // scale the output to obtain the exact inverse
    for (int i = 0; i < i_size; ++i) {
        out[i][REAL] /= i_size;
        out[i][IMAG] /= i_size;
    }
}

void FFT::conjugate(complexVect *out, int l_size)
{
    for(int i; i < l_size; ++i)
        out[i][IMAG] *= -1;
}


void FFT::writeFFTFile(char *i_fileName, complexVect *i_vector, int i_size, int complexRepresentation)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    char l_line[50], l_format[50];

    if (COMPLEX_REPRESENTATION)
        strcpy(l_format, "%.8lf %+.8lfi\n");
    else
        strcpy(l_format, "%.8lf %.8lf\n");


    for (int i = 0; i <i_size; ++i)
    {
        sprintf(l_line, l_format, i_vector[i][REAL], i_vector[i][IMAG]);
        myfile << l_line;
    }

    myfile << std::ends;
    myfile.close();
}

