#include "util.h"


void constructComplexVect(char * in, complexVect * out, int i_size)
{
    for (int i = 0; i < i_size; ++i)
    {
        out[i][REAL] = in[i];
        out[i][IMAG] = 0;
    }
}



void stdComplexToFftw(std::complex<double> *in, complexVect *out, int i_size)
{
    for(int i = 0; i < i_size; ++i)
    {
        out[i][REAL] = in[i].real();
        out[i][IMAG] = in[i].imag();
    }
}


void fftwToStdComplex(complexVect *in, std::complex<double> * out, int i_size)
{
    for(int i = 0; i < i_size; ++i)
        out[i] = std::complex <double> (in[i][REAL], in[i][IMAG]);
}


/*  (R1 + j I1) (R2 + j I2)
*   = R1R2 + R1I2 j + I1R2 j - I1I2
*   = (R1R2 - I1I2) + i (R1I2 + I1R2)
*/
void multFftw(complexVect const &n1, complexVect const &n2, complexVect res)
{
    res[REAL] = ( n1[REAL] * n2 [REAL] ) - ( n1[IMAG] * n2 [IMAG]);
    res[IMAG] = ( n1[REAL] * n2 [IMAG] ) + ( n1[IMAG] * n2 [REAL]);
}


void fileTextToStdComplexArray(char *i_fileName, std::complex<double> *l_out)
{
    FILE * fp = fopen(i_fileName, "r");
    const int l_bufferLength = 50;
    char l_buffer[l_bufferLength];
    double re, im;

    if(fp != NULL)
    {
        for (int i = 0; i < EXT_PRN_CODE_LENGTH * 10; ++i)
        {
            fgets(l_buffer, l_bufferLength, fp);
            sscanf (l_buffer,"%lf %lf", &re, &im);
            l_out[i] = std::complex<double>(re, im);
        }
    }
    else
        printf("Eroor while open %s", i_fileName);

}


void fileTextToFftwComplexArray(char *i_fileName, complexVect *l_out)
{
    FILE * fp = fopen(i_fileName, "r");
    const int l_bufferLength = 50;
    char l_buffer[l_bufferLength];
    double re, im;

    for (int i = 0; i < EXT_PRN_CODE_LENGTH * 10; ++i)
    {
        fgets(l_buffer, l_bufferLength, fp);
        sscanf (l_buffer,"%lf %lf", &re, &im);
        l_out[i][REAL] = re;
        l_out[i][IMAG] = im;
    }
}
