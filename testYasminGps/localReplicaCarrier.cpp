#include "localReplicaCarrier.h"
#include <cstring>
#include <fstream>

void LocalReplicaCarrier::phasePoints_X_dopplerFreq(std::complex <double> * o_vect, int i_doppFreqIndex)
{
    const int l_dopplerFreq = dopplerFreq[i_doppFreqIndex]; // l_dopplerFreq between 0..12499
    const double l_theta = (-2.0) * M_PI * ( 1.0 / 12500000.0) * l_dopplerFreq;

    for (int i = 0; i< EXT_PRN_CODE_LENGTH; ++i)
    {
        o_vect[i] = std::complex <double> (0, i * l_theta);
    }
}

void LocalReplicaCarrier::expCarrier(std::complex <double> * o_vect, int i_doppFreqIndex)
{
    phasePoints_X_dopplerFreq(o_vect, i_doppFreqIndex);

    for (int i = 0; i< EXT_PRN_CODE_LENGTH; ++i)
        o_vect[i] =  std::exp(o_vect[i]);
}

void LocalReplicaCarrier::writeExpCarrier(char *i_fileName, std::complex <double>  *i_vector, int i_size, int complexRepresentation)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    char l_line[50], l_format[50];

    if (COMPLEX_REPRESENTATION)
        strcpy(l_format, "%.8lf %+.8lfi\n");
    else
        strcpy(l_format, "%.8lf %.8lf\n");


    for (int i = 0; i <i_size; ++i)
    {
        sprintf(l_line, l_format,  i_vector[i].real(), i_vector[i].imag());
        myfile << l_line;
    }

    myfile << std::ends;
    myfile.close();
}
