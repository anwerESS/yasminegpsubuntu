
#include "fft.h"
#include "common.h"
#include "util.h"
#include "codePRNGenerator.h"
#include "localReplicaCarrier.h"
#include "aquisition.h"




int main()
{
/*
    // generate PRN
    char l_prnCode[PRN_CODE_LENGTH];
    codePRNGenerator::generatePRN(l_prnCode, codePRNGenerator::SV24);
    codePRNGenerator::writePrnToFile("./outSV24_1023.txt", l_prnCode, PRN_CODE_LENGTH);
    // generate ext PRN
    char l_extPrn[EXT_PRN_CODE_LENGTH];
    codePRNGenerator::generatePRN2(l_prnCode, l_extPrn);
    codePRNGenerator::writePrnToFile("./outSV24_12500.txt", l_extPrn, EXT_PRN_CODE_LENGTH);
*/

/*
    // input, output array
    complexVect l_inFFT[EXT_PRN_CODE_LENGTH], l_outFFt[EXT_PRN_CODE_LENGTH];

    constructComplexVect(l_extPrn, l_inFFT, EXT_PRN_CODE_LENGTH); // char(real) --> complexVect(real, im=0)
    // compute FFT
    FFT::fft(l_inFFT, l_outFFt, EXT_PRN_CODE_LENGTH, COMPLEX_CONJUGATED);

    FFT::writeFFTFile("./fftSV24.txt", l_outFFt, EXT_PRN_CODE_LENGTH, COMPLEX_REPRESENTATION);
*/
    /* calc exp carrier */
    std::complex<double> l_replCarr[12500];
    LocalReplicaCarrier::expCarrier(l_replCarr, 1);
    LocalReplicaCarrier::writeExpCarrier("expCarrier.txt", l_replCarr, EXT_PRN_CODE_LENGTH);
    /* calc incomming signal */
    std::complex<double> * l_incSign = new std::complex<double>[EXT_PRN_CODE_LENGTH_X_10];
    fileTextToStdComplexArray("./incommingSignal2.txt", l_incSign);

    /* incomming signal and replica multiplication*/
    std::complex<double> * l_mulIncSign_x_l_replCarr =  new std::complex<double>[EXT_PRN_CODE_LENGTH_X_10];
    Aquisition::incSig_x_carrReplic(l_replCarr, l_incSign, l_mulIncSign_x_l_replCarr);

    /* convert l_mulIncSign_x_l_replCarr (std::complex<float> ==> complexVect) to compute fft*/
    complexVect * l_incSignFftw = new complexVect [EXT_PRN_CODE_LENGTH_X_10];
    stdComplexToFftw(l_incSign, l_incSignFftw, EXT_PRN_CODE_LENGTH_X_10);
    /* free memory */
    delete [] l_incSign;
    delete [] l_mulIncSign_x_l_replCarr;
    /* fft */
    complexVect * l_outFftw = new complexVect[EXT_PRN_CODE_LENGTH_X_10];
    FFT::fft(l_incSignFftw, l_outFftw, EXT_PRN_CODE_LENGTH_X_10);
    FFT::writeFFTFile("./fftReplica.txt", l_outFftw, EXT_PRN_CODE_LENGTH_X_10);
    /* free memory */
    delete [] l_incSignFftw;
    delete [] l_outFftw;

    //////////////////////////// END //////////////////////////////

    /* do not forget to delete memory */
    return 0;
}
