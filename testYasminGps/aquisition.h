#ifndef AQUISITION_H
#define AQUISITION_H

#include "common.h"

class Aquisition
{
public:
    static void incSig_x_carrReplic(std::complex<double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<double> i_incSign[EXT_PRN_CODE_LENGTH*10],  std::complex<double> i_mulResult[EXT_PRN_CODE_LENGTH*10]);
};

#endif // AQUISITION_H
