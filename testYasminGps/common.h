#ifndef COMMON_H
#define COMMON_H

#include <fftw3.h>
#include <complex>

#define XOR ^

#define PRN_CODE_LENGTH             1023
#define EXT_PRN_CODE_LENGTH         12500
#define EXT_PRN_CODE_LENGTH_X_10    125000

#define REAL 0
#define IMAG 1

#define COMPLEX_NOT_CONJUGATED  0
#define COMPLEX_CONJUGATED      1

#define NORMAL_REPRESENTATION   0
#define COMPLEX_REPRESENTATION  1


#define complexVect fftw_complex


static short dopplerFreq[21] = {-5000, -4500, -4000, -3500, -3000, -2500,
                    -2000, -1500, -1000, -500, 0, 500, 1000,
                    1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000};




#endif // COMMON_H
