#include "aquisition.h"
#include <stdio.h>

void Aquisition::incSig_x_carrReplic(std::complex<double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<double> i_incSign[EXT_PRN_CODE_LENGTH*10], std::complex<double> o_mulResult[EXT_PRN_CODE_LENGTH*10])
{

    for (int row = 0; row <10; ++row)
    {
        for (int col = 0; col<EXT_PRN_CODE_LENGTH; ++col)
        {
            o_mulResult[col + row * EXT_PRN_CODE_LENGTH] = i_expCarr[col] * i_incSign[col + row * EXT_PRN_CODE_LENGTH];
        }
    }
}


