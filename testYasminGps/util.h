#ifndef UTIL_H
#define UTIL_H
#include "common.h"


void constructComplexVect(char * in, complexVect * out, int i_size);
void stdComplexToFftw(std::complex <double> * in, complexVect * out, int i_size);
void fftwToStdComplex(complexVect * in, std::complex <double> * out, int i_size);
void multFftw(complexVect const &n1, complexVect const &n2, complexVect &res);

void fileTextToStdComplexArray(char * i_fileName, std::complex<double> * l_out);
void fileTextToFftwComplexArray(char * i_fileName, complexVect * l_out);




#endif // UTIL_H
