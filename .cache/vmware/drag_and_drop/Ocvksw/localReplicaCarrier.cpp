#include "localReplicaCarrier.h"
#include <fstream>
#include <iostream>
#include <tgmath.h>


localReplicaCarrier::localReplicaCarrier()
{

}

void localReplicaCarrier::phasePoints_X_dopplerFreq(std::complex<long double> o_vect[EXT_PRN_CODE_LENGTH], int i_doppFreqIndex)
{
    const int l_dopplerFreq = dopplerFreq[i_doppFreqIndex];
    const long double l_theta = (-2) * PI * ( 1 / 12500000.0L) * l_dopplerFreq;
    for (int i = 0; i<EXT_PRN_CODE_LENGTH; ++i)
    {
        o_vect[i] =  std::complex<long double>(0, i * l_theta);
    }
}

void localReplicaCarrier::expCarrier(std::complex<long double> o_vect[EXT_PRN_CODE_LENGTH],  int i_doppFreqIndex)
{
    phasePoints_X_dopplerFreq(o_vect, i_doppFreqIndex);

    for (int i = 0; i<EXT_PRN_CODE_LENGTH; ++i)
    {
        o_vect[i] =  std::exp(o_vect[i]);
    }
}

void localReplicaCarrier::writeExpCarrier(char * i_fileName, std::complex<long double> * i_vector, int i_size)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    for (int i = 0; i <i_size; ++i)
    {
        char l_line[60];
        sprintf(l_line, "%Lf, %Lfi\n",i_vector[i].real(), i_vector[i].imag());
        myfile << l_line;
    }
    myfile << std::ends;
    myfile.close();
}
