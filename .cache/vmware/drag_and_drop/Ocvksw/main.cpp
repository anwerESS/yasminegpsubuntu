#include <iostream>
#include "codePRNGenerator.h"
#include "localReplicaCarrier.h"
#include "Aquisition.h"
#include "fft.h"

#include <fstream>
#include <complex>
#include <iostream>


using namespace std;



int main()
{


//    char l_prnCode[1023];
//    codePRNGenerator::generatePRN(l_prnCode, codePRNGenerator::SV24);
//    codePRNGenerator::writePrnToFile("outSV24_1023.txt", l_prnCode, PRN_CODE_LENGTH);

//    char l_extPrn[12500];
//    codePRNGenerator::generatePRN2(l_prnCode, l_extPrn);
//    codePRNGenerator::writePrnToFile("outSV24_12500.txt", l_extPrn, EXT_PRN_CODE_LENGTH);

//    std::complex<double> l_fft[EXT_PRN_CODE_LENGTH];
//    FFT::fft2(l_fft, l_extPrn, EXT_PRN_CODE_LENGTH);
//    FFT::conjugateVector(l_fft, EXT_PRN_CODE_LENGTH);
//    FFT::writeFFTFile("fftSV24.txt", l_fft, EXT_PRN_CODE_LENGTH);

    /* calc exp carrier */
    std::complex<long double> l_replCarr[12500];
    localReplicaCarrier::expCarrier(l_replCarr, 0);
    localReplicaCarrier::writeExpCarrier("expCarrier.txt", l_replCarr, EXT_PRN_CODE_LENGTH);
//    /* calc incomming signal */
//    std::complex<double> * l_incSign = new std::complex<double>[125000];
//    Aquisition::fileTextToComplexArray("incommingSignal2.txt", l_incSign);

//    /* incomming signal and replica multiplication*/
//    std::complex<double> * l_mulIncSign_x_l_replCarr =  new std::complex<double>[125000];
//    Aquisition::incSig_x_carrReplic(l_replCarr, l_incSign, l_mulIncSign_x_l_replCarr);
//    cout << l_mulIncSign_x_l_replCarr[0].real() << " " << l_mulIncSign_x_l_replCarr[0].imag() <<"\n";
//    cout << l_mulIncSign_x_l_replCarr[8].real() << " " << l_mulIncSign_x_l_replCarr[8].imag() <<"\n";

//    std::complex<double> * l_mulIncSign_x_l_replCarrFFT =  new std::complex<double>[125000];
//    FFT::fft3(l_mulIncSign_x_l_replCarrFFT, l_mulIncSign_x_l_replCarr, EXT_PRN_CODE_LENGTH*10);

//    for (int i = 0; i < 50; ++i)
//    {
//            cout << l_mulIncSign_x_l_replCarrFFT[i].real() << " " << l_mulIncSign_x_l_replCarrFFT[i].imag() <<"\n";
//    }


//    delete [] l_incSign;
//    delete [] l_mulIncSign_x_l_replCarr;

    return 0;
}
