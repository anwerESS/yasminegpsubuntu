#include "Aquisition.h"
#include <stdio.h>

Aquisition::Aquisition()
{

}

void Aquisition::incSig_x_carrReplic(std::complex<long double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<long double> i_incSign[EXT_PRN_CODE_LENGTH*10], std::complex<long double> o_mulResult[EXT_PRN_CODE_LENGTH*10])
{

    for (int row = 0; row <10; ++row)
    {
        for (int col = 0; col<EXT_PRN_CODE_LENGTH; ++col)
        {
            o_mulResult[col + row * EXT_PRN_CODE_LENGTH] = i_expCarr[col] * i_incSign[col + row * EXT_PRN_CODE_LENGTH];
        }
    }
}

void Aquisition::fileTextToComplexArray(char *i_fileName, std::complex<long double> * l_out)
{

    FILE * fp = fopen(i_fileName, "r");
    const int l_bufferLength = 50;
    char l_buffer[l_bufferLength];
    long double re, im;

    for (int i = 0; i < EXT_PRN_CODE_LENGTH * 10; ++i)
    {
        fgets(l_buffer, l_bufferLength, fp);
        sscanf (l_buffer,"%lf %lf", &re, &im);
        l_out[i] = std::complex<long double>(re, im);
    }

}

