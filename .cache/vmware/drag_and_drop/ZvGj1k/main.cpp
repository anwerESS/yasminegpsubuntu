#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void generateSquare(const char * inputFileName, const char * outputFileName);


int main()
{
    char const * inputFileName = "in.txt";
    char const * outputFileName = "out.txt";
    generateSquare(inputFileName, outputFileName);
    return 0;
}


void generateSquare(const char * inputFileName, const char * outputFileName)
{
    ifstream inputFile;
    ofstream outputFile;
    inputFile.open(inputFileName, ios::in);
    outputFile.open(outputFileName, ios::out);
    string line;
    int inValue, outValue;

    while (getline(inputFile , line))
    {
        inValue = stoi(line);
        outValue = inValue * inValue;
        outputFile << outValue <<"\n";
    }

    inputFile.close();
    outputFile.close();

}
