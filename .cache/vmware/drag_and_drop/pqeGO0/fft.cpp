#include "fft.h"
#include <fstream>
#include <valarray>
#include <cmath>

const double PI = 3.141592653589793238460;

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;

FFT::FFT()
{

}

void FFT::calcFFT(std::complex<double> o_res[EXT_PRN_CODE_LENGTH], char i_vect[EXT_PRN_CODE_LENGTH], int i_size)
{
    const int N = i_size;
    for (int k = 0; k<N; k++)
    {
        double realPart = 0.0;
        double imagPart = 0.0;
        for (int n = 0; n < i_size; ++n)
        {
            realPart += i_vect[n] * cos(((2*M_PI)/N) * k * n);
            imagPart -= i_vect[n] * sin(((2*M_PI)/N) * k * n);
        }
         o_res[k] = std::complex<double> (realPart, imagPart);
    }
}

void FFT::writeFFTFile(char * i_fileName, std::complex<double> * i_vector, int i_size)
{
    std::ofstream myfile;
    myfile.open (i_fileName, std::ios::out);

    for (int i = 0; i <i_size; ++i)
    {
        char l_line[30];
        sprintf(l_line, "%.4f %+.4fi\n", i_vector[i].real(), i_vector[i].imag());
        myfile << l_line;
    }
    myfile << std::ends;
    myfile.close();
}


////////////////////
void FFT::fft2(std::complex<double> o_res[EXT_PRN_CODE_LENGTH], char i_vect[EXT_PRN_CODE_LENGTH], int i_size)
{
    Complex l_vect[16384];
    for (int i = 0; i < i_size; ++i)
    {
        l_vect[i] = Complex(i_vect[i], 0);
    }

    for (int i = i_size; i < 16384; ++i)
    {
        l_vect[i] = Complex(0, 0);
    }


    CArray data(l_vect, 16384);
    // DFT
    unsigned int N = 16384, k = N, n;
    double thetaT = 3.14159265358979323846264338328L / N;
    Complex phiT = Complex(cos(thetaT), -sin(thetaT)), T;
    while (k > 1)
    {
        n = k;
        k >>= 1;
        phiT = phiT * phiT;
        T = 1.0L;
        for (unsigned int l = 0; l < k; l++)
        {
            for (unsigned int a = l; a < N; a += n)
            {
                unsigned int b = a + k;
                Complex t = data[a] - data[b];
                data[a] += data[b];
                data[b] = t * T;
            }
            T *= phiT;
        }
    }
    // Decimate
    unsigned int m = (unsigned int)log2(N);
    for (unsigned int a = 0; a < N; a++)
    {
        unsigned int b = a;
        // Reverse bits
        b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
        b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
        b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
        b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
        b = ((b >> 16) | (b << 16)) >> (32 - m);
        if (b > a)
        {
            Complex t = data[a];
            data[a] = data[b];
            data[b] = t;
        }
    }

    std::copy(begin(data), begin(data) +12500, o_res);

}

void FFT::fft3(std::complex<double> *o_res, std::complex<double> * i_vect, int i_size)
{
    const int l_size = pow(2, (int)(log(i_size)/log(2)) + 1);
    printf("l_size = %d\n",  l_size);
    Complex * l_vect = new Complex[l_size];
    for (int i = 0; i < i_size; ++i)
    {
        l_vect[i] = Complex(i_vect[i].real(), i_vect[i].imag());
    }

    for (int i = i_size; i < l_size; ++i)
    {
        l_vect[i] = Complex(0, 0);
    }

    CArray data(l_vect, l_size);
    // DFT
    unsigned int N = l_size, k = N, n;
    double thetaT = 3.14159265358979323846264338328L / N;
    Complex phiT = Complex(cos(thetaT), -sin(thetaT)), T;
    while (k > 1)
    {
        n = k;
        k >>= 1;
        phiT = phiT * phiT;
        T = 1.0L;
        for (unsigned int l = 0; l < k; l++)
        {
            for (unsigned int a = l; a < N; a += n)
            {
                unsigned int b = a + k;
                Complex t = data[a] - data[b];
                data[a] += data[b];
                data[b] = t * T;
            }
            T *= phiT;
        }
    }
    // Decimate
    unsigned int m = (unsigned int)log2(N);
    for (unsigned int a = 0; a < N; a++)
    {
        unsigned int b = a;
        // Reverse bits
        b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
        b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
        b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
        b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
        b = ((b >> 16) | (b << 16)) >> (32 - m);
        if (b > a)
        {
            Complex t = data[a];
            data[a] = data[b];
            data[b] = t;
        }
    }

    std::copy(begin(data), begin(data) +i_size, o_res);
}

void FFT::conjugateVector(std::complex<double> o_res[], int i_size)
{
    for (int i = 0; i < 12500; ++i)
        o_res[i] = std::conj(o_res[i]);
}

