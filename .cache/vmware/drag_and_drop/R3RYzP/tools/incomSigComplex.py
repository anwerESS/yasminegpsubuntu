import sys

inFile = sys.argv[1]
outFile = sys.argv[2]

print(inFile, outFile)

text = ""
L = []

with open(inFile, 'r') as f:
    text += f.read()

L_str = text.splitlines()
    
for line in L_str:
    line2 = line.replace("i", "j")
    c = complex(line2)
    L.append(c)
	
text = ""

for num in L:
    re = num.real
    im = num.imag
    text += "{} {}\n".format(re, im)
    
with open(outFile, 'w') as f:
    f.write(text)
