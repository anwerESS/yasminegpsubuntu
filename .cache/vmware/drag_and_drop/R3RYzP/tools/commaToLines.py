import sys

inFile = sys.argv[1]
outFile = sys.argv[2]

print(inFile, outFile)

text = ""

with open(inFile, 'r') as f:
    text += f.read()
    
text = text.replace(',', '\n')

with open(outFile, 'w') as f:
    f.write(text)
