/*! normalize.css 2012-03-11T12:53 UTC - http://github.com/necolas/normalize.css */

/* =============================================================================
   HTML5 display definitions
   ========================================================================== */

/*
 * Corrects block display not defined in IE6/7/8/9 & FF3
 */

article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
nav,
section,
summary {
    display: block;
}

/*
 * Corrects inline-block display not defined in IE6/7/8/9 & FF3
 */

audio,
canvas,
video {
    display: inline-block;
    *display: inline;
    *zoom: 1;
}

/*
 * Prevents modern browsers from displaying 'audio' without controls
 * Remove excess height in iOS5 devices
 */

audio:not([controls]) {
    display: none;
    height: 0;
}

/*
 * Addresses styling for 'hidden' attribute not present in IE7/8/9, FF3, S4
 * Known issue: no IE6 support
 */

[hidden] {
    display: none;
}


/* =============================================================================
   Base
   ========================================================================== */

/*
 * 1. Corrects text resizing oddly in IE6/7 when body font-size is set using em units
 *    http://clagnut.com/blog/348/#c790
 * 2. Prevents iOS text size adjust after orientation change, without disabling user zoom
 *    www.456bereastreet.com/archive/201012/controlling_text_size_in_safari_for_ios_without_disabling_user_zoom/
 */

html {
    font-size: 100%; /* 1 */
    -webkit-text-size-adjust: 100%; /* 2 */
    -ms-text-size-adjust: 100%; /* 2 */
}

/*
 * Addresses font-family inconsistency between 'textarea' and other form elements.
 */

html,
button,
input,
select,
textarea {
    font-family: sans-serif;
}

/*
 * Addresses margins handled incorrectly in IE6/7
 */

body {
    margin: 0;
}


/* =============================================================================
   Links
   ========================================================================== */

/*
 * Addresses outline displayed oddly in Chrome
 */

a:focus {
    outline: thin dotted;
}

/*
 * Improves readability when focused and also mouse hovered in all browsers
 * people.opera.com/patrickl/experiments/keyboard/test
 */

a:hover,
a:active {
    outline: 0;
}


/* =============================================================================
   Typography
   ========================================================================== */

/*
 * Addresses font sizes and margins set differently in IE6/7
 * Addresses font sizes within 'section' and 'article' in FF4+, Chrome, S5
 */

h1 {
    font-size: 2em;
    margin: 0.67em 0;
}

h2 {
    font-size: 1.4em;
    margin: 0.83em 0;
}

h3 {
    font-size: 1.17em;
    margin: 1em 0;
}

h4 {
    font-size: 1em;
    margin: 1.33em 0;
}

h5 {
    font-size: 0.83em;
    margin: 1.67em 0;
}

h6 {
    font-size: 0.75em;
    margin: 2.33em 0;
}

/*
 * Addresses styling not present in IE7/8/9, S5, Chrome
 */

abbr[title] {
    border-bottom: 1px dotted;
}

/*
 * Addresses style set to 'bolder' in FF3+, S4/5, Chrome
*/

b,
strong {
    font-weight: bold;
}

blockquote {
    margin: 1em 40px;
}

/*
 * Addresses styling not present in S5, Chrome
 */

dfn {
    font-style: italic;
}

/*
 * Addresses styling not present in IE6/7/8/9
 */

mark {
    background: #ff0;
    color: #000;
}

/*
 * Addresses margins set differently in IE6/7
 */

p,
pre {
    margin: 1em 0;
}

/*
 * Corrects font family set oddly in IE6, S4/5, Chrome
 * en.wikipedia.org/wiki/User:Davidgothberg/Test59
 */

pre,
code,
kbd,
samp {
    font-family: monospace, serif;
    _font-family: 'courier new', monospace;
    font-size: 1em;
}

/*
 * Improves readability of pre-formatted text in all browsers
 */

pre {
    white-space: pre;
    white-space: pre-wrap;
    word-wrap: break-word;
}

/*
 * 1. Addresses CSS quotes not supported in IE6/7
 * 2. Addresses quote property not supported in S4
 */

/* 1 */

q {
    quotes: none;
}

/* 2 */

q:before,
q:after {
    content: '';
    content: none;
}

small {
    font-size: 75%;
}

/*
 * Prevents sub and sup affecting line-height in all browsers
 * gist.github.com/413930
 */

sub,
sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
}

sup {
    top: -0.5em;
}

sub {
    bottom: -0.25em;
}


/* =============================================================================
   Lists
   ========================================================================== */

/*
 * Addresses margins set differently in IE6/7
 */

dl,
menu,
ol,
ul {
    margin: 1em 0;
}

dd {
    margin: 0 0 0 40px;
}

/*
 * Addresses paddings set differently in IE6/7
 */

menu,
ol,
ul {
    padding: 0 0 0 40px;
}

/*
 * Corrects list images handled incorrectly in IE7
 */

nav ul,
nav ol {
    list-style: none;
    list-style-image: none;
}


/* =============================================================================
   Embedded content
   ========================================================================== */

/*
 * 1. Removes border when inside 'a' element in IE6/7/8/9, FF3
 * 2. Improves image quality when scaled in IE7
 *    code.flickr.com/blog/2008/11/12/on-ui-quality-the-little-things-client-side-image-resizing/
 */

img {
    border: 0; /* 1 */
    -ms-interpolation-mode: bicubic; /* 2 */
}

/*
 * Corrects overflow displayed oddly in IE9
 */

svg:not(:root) {
    overflow: hidden;
}


/* =============================================================================
   Figures
   ========================================================================== */

/*
 * Addresses margin not present in IE6/7/8/9, S5, O11
 */

figure {
    margin: 0;
}


/* =============================================================================
   Forms
   ========================================================================== */

/*
 * Corrects margin displayed oddly in IE6/7
 */

form {
    margin: 0;
}

/*
 * Define consistent border, margin, and padding
 */

fieldset {
    border: 1px solid #c0c0c0;
    margin: 0 2px;
    padding: 0.35em 0.625em 0.75em;
}

/*
 * 1. Corrects color not being inherited in IE6/7/8/9
 * 2. Corrects text not wrapping in FF3
 * 3. Corrects alignment displayed oddly in IE6/7
 */

legend {
    border: 0; /* 1 */
    padding: 0;
    white-space: normal; /* 2 */
    *margin-left: -7px; /* 3 */
}

/*
 * 1. Corrects font size not being inherited in all browsers
 * 2. Addresses margins set differently in IE6/7, FF3+, S5, Chrome
 * 3. Improves appearance and consistency in all browsers
 */

button,
input,
select,
textarea {
    font-size: 100%; /* 1 */
    margin: 0; /* 2 */
    vertical-align: baseline; /* 3 */
    *vertical-align: middle; /* 3 */
}

/*
 * Addresses FF3/4 setting line-height on 'input' using !important in the UA stylesheet
 */

button,
input {
    line-height: normal; /* 1 */
}

/*
 * 1. Improves usability and consistency of cursor style between image-type 'input' and others
 * 2. Corrects inability to style clickable 'input' types in iOS
 * 3. Removes inner spacing in IE7 without affecting normal text inputs
 *    Known issue: inner spacing remains in IE6
 */

button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
    cursor: pointer; /* 1 */
    -webkit-appearance: button; /* 2 */
    *overflow: visible;  /* 3 */
}

/*
 * Re-set default cursor for disabled elements
 */

button[disabled],
input[disabled] {
    cursor: default;
}

/*
 * 1. Addresses box sizing set to content-box in IE8/9
 * 2. Removes excess padding in IE8/9
 * 3. Removes excess padding in IE7
      Known issue: excess padding remains in IE6
 */

input[type="checkbox"],
input[type="radio"] {
    box-sizing: border-box; /* 1 */
    padding: 0; /* 2 */
    *height: 13px; /* 3 */
    *width: 13px; /* 3 */
}

/*
 * 1. Addresses appearance set to searchfield in S5, Chrome
 * 2. Addresses box-sizing set to border-box in S5, Chrome (include -moz to future-proof)
 */

input[type="search"] {
    -webkit-appearance: textfield; /* 1 */
    -moz-box-sizing: content-box;
    -webkit-box-sizing: content-box; /* 2 */
    box-sizing: content-box;
}

/*
 * Removes inner padding and search cancel button in S5, Chrome on OS X
 */

input[type="search"]::-webkit-search-decoration,
input[type="search"]::-webkit-search-cancel-button {
    -webkit-appearance: none;
}

/*
 * Removes inner padding and border in FF3+
 * www.sitepen.com/blog/2008/05/14/the-devils-in-the-details-fixing-dojos-toolbar-buttons/
 */

button::-moz-focus-inner,
input::-moz-focus-inner {
    border: 0;
    padding: 0;
}

/*
 * 1. Removes default vertical scrollbar in IE6/7/8/9
 * 2. Improves readability and alignment in all browsers
 */

textarea {
    overflow: auto; /* 1 */
    vertical-align: top; /* 2 */
}


/* =============================================================================
   Tables
   ========================================================================== */

/*
 * Remove most spacing between table cells
 */

table {
    border-collapse: collapse;
    border-spacing: 0;
}
C��7      _[$_[$A�_��   ,    :https://download.qt.io/static/normalize.css necko:classified 1 strongly-framed 1 security-info FnhllAKWRHGAlo+ESXykKAAAAAAAAAAAwAAAAAAAAEaphjojH6pBabDSgSnsfLHeAAgAAgAAAAAAAAAAAAAAAAAAAAEAMQFmCjImkVxP+7sgiYWmMt8FvcOXmlQiTNWFiWlrbpbqgwAAAAAAAAUOMIIFCjCCA/KgAwIBAgIQA3LWpR5gvPlx+ZIclWIl/DANBgkqhkiG9w0BAQsFADBNMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5EaWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMTcwODI5MDAwMDAwWhcNMjAxMDIxMTIwMDAwWjBMMQswCQYDVQQGEwJGSTEOMAwGA1UEBxMFRXNwb28xGzAZBgNVBAoTElRoZSBRdCBDb21wYW55IEx0ZDEQMA4GA1UEAwwHKi5xdC5pbzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALDOo5wRt7yA/Zjg2QZkksXN+ie907nOcEBC4H8BdeiQVdv5xw6iw7MP8F9xaTqWUPeclVGtAQO6T8R5UZG/ZGH1wogPJ7j1WiVFZWCt2wKKBQ0apVD2w7Kh/7UwqFDpsujBzPItbiwBBX/rhrerAjMOJTin8kJV1KJUV+kVqBuUdSZRAtBs5Oaw1JzNtAD2jNvEj4rDKPqu28HS4OlxuUOU51ib9OQOEeUkkP7gedcbdGjPszM9PBRLXDknQgGFiyN5q54BDyK2feW3GbPZ+ld3Qir6kc2ALqaPLfGBm0/kJJH1NRIWbhoXqZ0oKLomS2Nwt5fHJY0wbppH2M3Z9yMCAwEAAaOCAeUwggHhMB8GA1UdIwQYMBaAFA+AYRyCMWHVLyjnjUY4tCzhxtniMB0GA1UdDgQWBBSjlyIuquDo8RS8ajegd06uuphWFDApBgNVHREEIjAgggcqLnF0LmlvggVxdC5pb4IOZG93bmxvYWQucXQuaW8wDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjBrBgNVHR8EZDBiMC+gLaArhilodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vc3NjYS1zaGEyLWcxLmNybDAvoC2gK4YpaHR0cDovL2NybDQuZGlnaWNlcnQuY29tL3NzY2Etc2hhMi1nMS5jcmwwTAYDVR0gBEUwQzA3BglghkgBhv1sAQEwKjAoBggrBgEFBQcCARYcaHR0cHM6Ly93d3cuZGlnaWNlcnQuY29tL0NQUzAIBgZngQwBAgIwfAYIKwYBBQUHAQEEcDBuMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wRgYIKwYBBQUHMAKGOmh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFNIQTJTZWN1cmVTZXJ2ZXJDQS5jcnQwDAYDVR0TAQH/BAIwADANBgkqhkiG9w0BAQsFAAOCAQEAVDiNruBNosqXdXSLBtHiKGeXSxaTLAc4gHFNElcRByRqgxJRh1zmwFYoekc4UEIAQAGabC6BZpzqqZ3Y7k5PfNffGomPhBV/Gey86TpkSGzuR6rFxx+pBOI0v7QMZQOlfie55EAF/MO94xPMZ+J2uQWK1chm4X7voRO8k+pDfBnYgCEtJht2idXjkCeg/NhoI1oUxm6qhd6mCIJVQv/W89DpnPs34W2KVQH6BWfrbyEQ46MqYatZmOqY65dRsZONDwyIpB+0EKKqL4aSGxa7O0QKZHVgwtg6WhBQkH9DWOW1912Oq1nZGiUCkHN1YUJZd1AI1ixHxCP4KZ69C7m41sAvAAMAAAAAAQEAAAAAAAAEUDI1NgAAABBSU0EtUEtDUzEtU0hBMjU2AZWfsWVlF0h/q5vYkTvlMZeudM2lzS9HP5b18Lf/9ixoAAAAA2YKMiaRXE/7uyCJhaYy3wW9w5eaVCJM1YWJaWtuluqDAAAAAAAABQ4wggUKMIID8qADAgECAhADctalHmC8+XH5khyVYiX8MA0GCSqGSIb3DQEBCwUAME0xCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxJzAlBgNVBAMTHkRpZ2lDZXJ0IFNIQTIgU2VjdXJlIFNlcnZlciBDQTAeFw0xNzA4MjkwMDAwMDBaFw0yMDEwMjExMjAwMDBaMEwxCzAJBgNVBAYTAkZJMQ4wDAYDVQQHEwVFc3BvbzEbMBkGA1UEChMSVGhlIFF0IENvbXBhbnkgTHRkMRAwDgYDVQQDDAcqLnF0LmlvMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsM6jnBG3vID9mODZBmSSxc36J73Tuc5wQELgfwF16JBV2/nHDqLDsw/wX3FpOpZQ95yVUa0BA7pPxHlRkb9kYfXCiA8nuPVaJUVlYK3bAooFDRqlUPbDsqH/tTCoUOmy6MHM8i1uLAEFf+uGt6sCMw4lOKfyQlXUolRX6RWoG5R1JlEC0Gzk5rDUnM20APaM28SPisMo+q7bwdLg6XG5Q5TnWJv05A4R5SSQ/uB51xt0aM+zMz08FEtcOSdCAYWLI3mrngEPIrZ95bcZs9n6V3dCKvqRzYAupo8t8YGbT+QkkfU1EhZuGhepnSgouiZLY3C3l8cljTBumkfYzdn3IwIDAQABo4IB5TCCAeEwHwYDVR0jBBgwFoAUD4BhHIIxYdUvKOeNRji0LOHG2eIwHQYDVR0OBBYEFKOXIi6q4OjxFLxqN6B3Tq66mFYUMCkGA1UdEQQiMCCCByoucXQuaW+CBXF0Lmlvgg5kb3dubG9hZC5xdC5pbzAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMGsGA1UdHwRkMGIwL6AtoCuGKWh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9zc2NhLXNoYTItZzEuY3JsMC+gLaArhilodHRwOi8vY3JsNC5kaWdpY2VydC5jb20vc3NjYS1zaGEyLWcxLmNybDBMBgNVHSAERTBDMDcGCWCGSAGG/WwBATAqMCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5jb20vQ1BTMAgGBmeBDAECAjB8BggrBgEFBQcBAQRwMG4wJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBGBggrBgEFBQcwAoY6aHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0U0hBMlNlY3VyZVNlcnZlckNBLmNydDAMBgNVHRMBAf8EAjAAMA0GCSqGSIb3DQEBCwUAA4IBAQBUOI2u4E2iypd1dIsG0eIoZ5dLFpMsBziAcU0SVxEHJGqDElGHXObAVih6RzhQQgBAAZpsLoFmnOqpndjuTk98198aiY+EFX8Z7LzpOmRIbO5HqsXHH6kE4jS/tAxlA6V+J7nkQAX8w73jE8xn4na5BYrVyGbhfu+hE7yT6kN8GdiAIS0mG3aJ1eOQJ6D82GgjWhTGbqqF3qYIglVC/9bz0Omc+zfhbYpVAfoFZ+tvIRDjoyphq1mY6pjrl1Gxk40PDIikH7QQoqovhpIbFrs7RApkdWDC2DpaEFCQf0NY5bX3XY6rWdkaJQKQc3VhQll3UAjWLEfEI/gpnr0LubjWZgoyJpFcT/u7IImFpjLfBb3Dl5pUIkzVhYlpa26W6oMAAAAAAAAEmDCCBJQwggN8oAMCAQICEAH9o+tuynXIiEOLckvPvJEwDQYJKoZIhvcNAQELBQAwYTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTEgMB4GA1UEAxMXRGlnaUNlcnQgR2xvYmFsIFJvb3QgQ0EwHhcNMTMwMzA4MTIwMDAwWhcNMjMwMzA4MTIwMDAwWjBNMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5EaWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDcrliQTcHEMBWQNVtuPIIV9SxcvePb/3FD+mQlgNTuGKJN8GbQCnNuEZg2F2SvN539+kGEr8evjP4ac03PM5eQopaHU4MruaZ1SC0dVjd72jEyGtesqwb0ql1Lt0dG3SqTw5AueYCA7xMEahQ7tZuSvsIHZU782vz/eq7cXH5VMQzoOQek174v0wtq0rHfX/5XdFM7NYDdro5EmLOfDtPa4Nf0aymrRKdLWIRtkkuBw9pzixKXSJAERXUa3Tcxl5LozVQNO+TBPzleLrjzXH4QjoZBAI1FZkewoWXOoKopCU7zl+voLqsPcqcwDvrH9P0Ud8OkWyhXwrP5gv23RVibAgMBAAGjggFaMIIBVjASBgNVHRMBAf8ECDAGAQH/AgEAMA4GA1UdDwEB/wQEAwIBhjA0BggrBgEFBQcBAQQoMCYwJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTB7BgNVHR8EdDByMDegNaAzhjFodHRwOi8vY3JsMy5kaWdpY2VydC5jb20vRGlnaUNlcnRHbG9iYWxSb290Q0EuY3JsMDegNaAzhjFodHRwOi8vY3JsNC5kaWdpY2VydC5jb20vRGlnaUNlcnRHbG9iYWxSb290Q0EuY3JsMD0GA1UdIAQ2MDQwMgYEVR0gADAqMCgGCCsGAQUFBwIBFhxodHRwczovL3d3dy5kaWdpY2VydC5jb20vQ1BTMB0GA1UdDgQWBBQPgGEcgjFh1S8o541GOLQs4cbZ4jAfBgNVHSMEGDAWgBQD3lA1VtFMu2bwo+IbG8OXsj3RVTANBgkqhkiG9w0BAQsFAAOCAQEAIz7fS9IxQqW2fkJcGkTMadFotF1L4AQhbEvibcyx4JePplMJzaoqZeU5Tx6DpW5cmKIkJub7oe2Txy4Cxk1Kv7BC33jas6j5bf8hhVM2YEx2zuw43NZRgPDF1uXUTSdkq5vHPnH7SJe4M23JEwfulqIbGBX2XExA7bPC7P9xweNH/9S5ALQ3Qtogyepuiu4UBq59olmYiKgbby308skUXybPLI1+7TfAqdU5uYK/GQzqNK8AIWj4rXPiyTLaOCULVdOaHfBohu0uQTTvfKVQHb86+dPBCAzm7R6KWCXkuHetLW71Ut20dI+rSS6dO5M0KB94zpTqx73TyW0c3lwy82YKMiaRXE/7uyCJhaYy3wW9w5eaVCJM1YWJaWtuluqDAAAAAAAAA7MwggOvMIICl6ADAgECAhAIO+BWkEJGsaF1aslZkcdKMA0GCSqGSIb3DQEBBQUAMGExCzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5jb20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IENBMB4XDTA2MTExMDAwMDAwMFoXDTMxMTExMDAwMDAwMFowYTELMAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNvbTEgMB4GA1UEAxMXRGlnaUNlcnQgR2xvYmFsIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDiO+ERct6opNOjV6pQoo8Ld5DJoqXuEs6WWwEJIMwBk6dOMLdT90PEaQBXneKNIt2HBkAAgQnOzhuDv9/NO3FG4tZmxwWzdicWj3ueHpV97rdIowja1q96DDkGZX9KXR+8F/irvu4o13R/eniZWYVoblwjMku/TsDoWm3jcL93EL/8AfaF2ahEEFgyqXUY1dGivkfiJ2r0mjP4SQhgi9RftDqEv6GqSkx9Ps9PX2x2XqBLN5Ge3CLmbc4UGo5qy/7NsxRkF8dbKZ4yv/Lu+tMLQtSrt0Ey2gzU7/iB1buNWD+1G+hJKKJw2jEE3feyFvJMCk4HqO1KPV61f6OQw68nAgMBAAGjYzBhMA4GA1UdDwEB/wQEAwIBhjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQD3lA1VtFMu2bwo+IbG8OXsj3RVTAfBgNVHSMEGDAWgBQD3lA1VtFMu2bwo+IbG8OXsj3RVTANBgkqhkiG9w0BAQUFAAOCAQEAy5w3qkgTEgr63UScT1Kw9N+uBPV5eQijJBj8SyuEwC251cf+9MEfWMu4bZx6dOeYKasRteNwoKHNTIiZk4yRcOKrDxy+k6n/Y9XkB2DTo7+dWwnx1Y7jU/SOY/o/p9u0Zt9iZtbRbkGN8i216ndKn51Y4itZwEAj7S0ogkU+eVSSJpjggEioN+/w1nlgFt6s6A7NbqxEFzgvSdrhRT4quTZTzzpQBvcu6MRXSWxhIRjVBK14PCw6gGun668VFOnYicG5OGzikWyK/2S5dyVXMMAbJKPh3OnfR3y1tCQIBTDsLb0Lv0W/ULmp8+uYARKtyIjGmDRfjQo8xunVlZVt3gA= request-method GET response-head HTTP/1.1 200 OK
Date: Sat, 18 Jul 2020 02:15:09 GMT
Server: Apache
Last-Modified: Wed, 13 Feb 2013 10:36:05 GMT
ETag: "34194-2362-4d598b5948f40"
Accept-Ranges: bytes
Content-Length: 9058
Content-Type: text/css
 original-response-headers Date: Sat, 18 Jul 2020 02:15:09 GMT
Server: Apache
Last-Modified: Wed, 13 Feb 2013 10:36:05 GMT
ETag: "34194-2362-4d598b5948f40"
Accept-Ranges: bytes
Content-Length: 9058
Keep-Alive: timeout=15, max=99
Connection: Keep-Alive
Content-Type: text/css
 uncompressed-len 0 net-response-time-onstart 135 net-response-time-onstop 137   #b