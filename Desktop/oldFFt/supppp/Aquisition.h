#ifndef AQUISITION_H
#define AQUISITION_H

#include "common.h"
#include <complex>

class Aquisition
{
public:
    Aquisition();
    static void incSig_x_carrReplic(std::complex<long double> i_expCarr[EXT_PRN_CODE_LENGTH], std::complex<long double> i_incSign[EXT_PRN_CODE_LENGTH*10],  std::complex<long double> i_mulResult[EXT_PRN_CODE_LENGTH*10]);
    static void fileTextToComplexArray(char * i_fileName, std::complex<long double> * l_out);
};

#endif // AQUISITION_H
