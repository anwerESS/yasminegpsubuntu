#ifndef COMMON_H
#define COMMON_H

#define XOR ^

#define PRN_CODE_LENGTH         1023
#define EXT_PRN_CODE_LENGTH     12500

static short dopplerFreq[21] = {-5000, -4500, -4000, -3500, -3000, -2500,
                    -2000, -1500, -1000, -500, 0, 500, 1000,
                    1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000};


const long double PI = 3.14159265358979323846264338328L;


#endif // COMMON_H
