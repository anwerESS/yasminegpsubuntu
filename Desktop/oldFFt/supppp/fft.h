#ifndef FFT_H
#define FFT_H

#include "common.h"
#include <complex>

class FFT
{
public:
    FFT();

    static void calcFFT(std::complex<long double> o_res[EXT_PRN_CODE_LENGTH], char i_vect[EXT_PRN_CODE_LENGTH], int i_size);
    static void writeFFTFile(char * i_fileName, std::complex<long double> * i_vector, int i_size);

    static void fft2(std::complex<long double> o_res[EXT_PRN_CODE_LENGTH], char i_vect[EXT_PRN_CODE_LENGTH], int i_size);
    static void fft3(std::complex<long double> * o_res, std::complex<long double> * i_vect, int i_size);
    static void conjugateVector(std::complex<long double> o_res[EXT_PRN_CODE_LENGTH], int i_size);


};

#endif // FFT_H
