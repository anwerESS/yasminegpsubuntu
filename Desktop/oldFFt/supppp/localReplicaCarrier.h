#ifndef LOCALREPLICACARRIER_H
#define LOCALREPLICACARRIER_H

#include<complex>
#include<cmath>
#include "common.h"

class localReplicaCarrier
{
public:
    localReplicaCarrier();
    static void phasePoints_X_dopplerFreq(std::complex<long double> o_vect[EXT_PRN_CODE_LENGTH], int i_doppFreqIndex);
    static void expCarrier(std::complex<long double> o_vect[EXT_PRN_CODE_LENGTH],  int i_doppFreqIndex);
    static void writeExpCarrier(char * i_fileName, std::complex<long double> * i_vector, int i_size);

};

#endif // LOCALREPLICACARRIER_H
