#ifndef CODEPRNGENERATOR_H
#define CODEPRNGENERATOR_H

#include "common.h"
#include <fstream>



static const int sv[][2]={
    {2, 6},
    {3, 7},
    {4, 8},
    {5, 9},
    {1, 9},
    {2, 10},
    {1, 8},
    {2, 9},
    {3, 10},
    {2, 3},
    {3, 4},
    {5, 6},
    {6, 7},
    {7, 8},
    {8, 9},
    {9, 10},
    {1, 4},
    {2, 5},
    {3, 6},
    {4, 7},
    {5, 8},
    {6, 9},
    {1, 3},
    {4, 6},
    {5, 7},
    {6, 8},
    {7, 9},
    {8, 10},
    {1, 6},
    {2, 7},
    {3, 8},
    {4, 9},
    {5, 1},
    {4, 10},
    {1, 7},
    {2, 8},
    {4, 10},
};


class codePRNGenerator
{
public:
    enum SV{SV1, SV2, SV3, SV4, SV5, SV6, SV7, SV8, SV9, SV10, SV11, SV12, SV13, SV14, SV15, SV16,
                      SV17, SV18, SV19, SV20, SV21, SV22, SV23, SV24, SV25, SV26, SV27, SV28, SV29, SV30, SV31, SV32};

    codePRNGenerator();
    static void shiftRightPol(bool i_pol[]);
    static bool outPol1();
    static bool outPol2(int i_phaseSel1, int i_phaseSel2);
    static bool outBufferBit(int l_sv);
    static void resetPols();

    static void generatePRN(char l_out[PRN_CODE_LENGTH], int l_sv);
    static void generatePRN2(char i_prn[EXT_PRN_CODE_LENGTH], char * l_out);

    static void writePrnToFile(char * i_fileName, char * i_vector, int i_size);

private:
    static bool m_pol1[10];
    static bool m_pol2[10];
};

#endif // CODEPRNGENERATOR_H
