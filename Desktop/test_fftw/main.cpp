
#include "fft.h"

double realTab[5] = {1.0, 2.0, 3.0, 4.0, 5.0};
double imTab[5] = {11.0, 12.0, 13.0, 14.0, 15.0};

int main()
{
	int n = 5;
	
	// input array
	fftw_complex x[n];
	// output array
	fftw_complex y[n];
	// fill the first array with some numbers
	for (int i = 0; i < n; ++i) {
		x[i][REAL] = realTab[i];
		x[i][IMAG] = imTab[i];
	}
	
	fft(x, y, n);
	displayComplex(y, n);







/*
		// create a DFT plan
	fftw_plan plan = fftw_plan_dft_1d(n, x, y, FFTW_FORWARD, FFTW_ESTIMATE);
	// execute the plan
	fftw_execute(plan);
	// do some cleaning
	fftw_destroy_plan(plan);
	fftw_cleanup();

	for (int i = 0; i < n; ++i)
		if (y[i][IMAG] < 0)
			cout << y[i][REAL] << " - " << abs(y[i][IMAG]) << "i" << endl;
		else
			cout << y[i][REAL] << " + " << y[i][IMAG] << "i" << endl;
			*/
	
	return 0;
}
