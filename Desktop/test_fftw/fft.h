
#include <fftw3.h>
#include <iostream>
#include <cmath>

using namespace std;

// macros for the real and imaginary parts
#define REAL 0
#define IMAG 1

void fft(fftw_complex *in, fftw_complex *out, int size);
void ifft(fftw_complex *in, fftw_complex *out, int size);
void displayComplex(fftw_complex *y, int size);