#include "fft.h"



void fft(fftw_complex *in, fftw_complex *out, int size)
{
	// create a DFT plan
	fftw_plan plan = fftw_plan_dft_1d(size, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	// execute the plan
	fftw_execute(plan);
	// do some cleaning
	fftw_destroy_plan(plan);
	fftw_cleanup();
}

void ifft(fftw_complex *in, fftw_complex *out, int size)
{
	// create an IDFT plan
	fftw_plan plan = fftw_plan_dft_1d(size, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
	// execute the plan
	fftw_execute(plan);
	// do some cleaning
	fftw_destroy_plan(plan);
	fftw_cleanup();
	// scale the output to obtain the exact inverse
	for (int i = 0; i < size; ++i) {
		out[i][REAL] /= size;
		out[i][IMAG] /= size;
	}
}

/* Displays complex numbers in the form a +/- bi. */
void displayComplex(fftw_complex *y, int size)
{
	for (int i = 0; i < size; ++i)
		if (y[i][IMAG] < 0)
			cout << y[i][REAL] << " - " << abs(y[i][IMAG]) << "i" << endl;
		else
			cout << y[i][REAL] << " + " << y[i][IMAG] << "i" << endl;
}
