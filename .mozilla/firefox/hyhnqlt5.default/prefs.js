// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.user_id", "34304a43-3039-4fbe-8734-5a11bf77c174");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1595122569);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 1595122689);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1595122050);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1595122290);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1595122170);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1595122449);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1595036529);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1595122809);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.capacity", 716800);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.download.panel.shown", true);
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1595035659);
user_pref("browser.laterrun.bookkeeping.sessionCount", 2);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 77);
user_pref("browser.newtabpage.activity-stream.impressionId", "{4ea8a1db-4b1e-4b2f-b3de-ec6fc2cba128}");
user_pref("browser.newtabpage.activity-stream.migrationLastShownDate", 1594969200);
user_pref("browser.newtabpage.activity-stream.migrationRemainingDays", 3);
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1595124036130");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1595125828130");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1595122059111");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1595125659111");
user_pref("browser.search.region", "TN");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20190128171555");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1595041395");
user_pref("browser.slowStartup.averageTime", 19840);
user_pref("browser.slowStartup.samples", 2);
user_pref("browser.startup.homepage_override.buildID", "20190128171555");
user_pref("browser.startup.homepage_override.mstone", "65.0");
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\"],\"currentVersion\":15,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.timesBeforeHidingSuggestionsHint", 0);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1595041403456");
user_pref("devtools.onboarding.telemetry.logged", true);
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "65.0");
user_pref("distribution.iniFile.exists.value", true);
user_pref("dom.push.userAgentID", "e379fccf8ddd4ee392fa54f8a4a85bfc");
user_pref("extensions.blocklist.lastModified", "Fri, 17 Jul 2020 10:20:06 GMT");
user_pref("extensions.blocklist.pingCountTotal", 3);
user_pref("extensions.blocklist.pingCountVersion", 3);
user_pref("extensions.databaseSchema", 28);
user_pref("extensions.getAddons.cache.lastUpdate", 1595122570);
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.lastAppBuildId", "20190128171555");
user_pref("extensions.lastAppVersion", "65.0");
user_pref("extensions.lastPlatformVersion", "65.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.uuids", "{\"formautofill@mozilla.org\":\"e4f6fd81-27d0-4f3f-81ad-b9f9d3bc6f41\",\"screenshots@mozilla.org\":\"3ad6a8e7-8dca-4cda-9e26-9818e78387ad\",\"webcompat-reporter@mozilla.org\":\"018ce5f0-e38b-43b9-a5ee-bdc166fb3fab\",\"webcompat@mozilla.org\":\"5996437b-124f-4a30-a3ed-1d26a34c5f1b\",\"baidu-code-update@mozillaonline.com\":\"e5941000-de66-40ed-a19d-e1403d816503\",\"fxmonitor@mozilla.org\":\"fcc674c1-a1ba-4a75-a24d-ca9eb4cf884f\"}");
user_pref("font.internaluseonly.changed", true);
user_pref("idle.lastDailyNotification", 1595036289);
user_pref("lightweightThemes.usedThemes", "[]");
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1595035812);
user_pref("media.gmp-gmpopenh264.version", "1.7.1");
user_pref("media.gmp-manager.buildID", "20190128171555");
user_pref("media.gmp-manager.lastCheck", 1595035809);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("network.predictor.cleaned-up", true);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("pdfjs.previousHandler.alwaysAskBeforeHandling", true);
user_pref("pdfjs.previousHandler.preferredAction", 4);
user_pref("places.database.lastMaintenance", 1595036289);
user_pref("places.history.expiration.transient_current_max_pages", 112348);
user_pref("plugin.disable_full_page_plugin_for_types", "application/pdf");
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("security.sandbox.content.tempDirSuffix", "f4fff709-f057-4293-baab-504d4cb8ae82");
user_pref("security.sandbox.plugin.tempDirSuffix", "4fdaa5b3-3d34-47ec-9a14-4a7a823a6f44");
user_pref("services.blocklist.addons.checked", 1595122513);
user_pref("services.blocklist.gfx.checked", 1595122513);
user_pref("services.blocklist.onecrl.checked", 1595122513);
user_pref("services.blocklist.pinning.checked", 1595122513);
user_pref("services.blocklist.plugins.checked", 1595122513);
user_pref("services.settings.clock_skew_seconds", -64);
user_pref("services.settings.last_etag", "\"1595102075079\"");
user_pref("services.settings.last_update_seconds", 1595122513);
user_pref("services.settings.main.language-dictionaries.last_check", 1595122513);
user_pref("services.settings.main.sites-classification.last_check", 1595122513);
user_pref("services.settings.main.tippytop.last_check", 1595122513);
user_pref("signon.importedFromSqlite", true);
user_pref("storage.vacuum.last.index", 0);
user_pref("storage.vacuum.last.places.sqlite", 1595036289);
user_pref("toolkit.startup.last_success", 1595041387);
user_pref("toolkit.telemetry.cachedClientID", "0a9f6bdc-2856-4bf0-a004-6beda2d7a8be");
user_pref("toolkit.telemetry.previousBuildID", "20190128171555");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
